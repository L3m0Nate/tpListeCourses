<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="">
	    <meta name="author" content="">
	
	    <title>Listes</title>
	
	    <!-- Bootstrap core CSS -->
	    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	    <!-- Custom styles for this template -->
	    <link href="css/4-col-portfolio.css" rel="stylesheet">
	
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	
	<body class="container">
		<header class="py-3 bg-dark header-demodule fixed-top">
		    <div class="container text-center text-white">
		        <h1>Courses</h1>
		    </div>
		</header>
		
		<!-- Confirmation modal -->
		<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Supprimer une liste</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        Souhaitez-vous réellement supprimer ${l.nom}?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
			        <button data-id="${l.id}" type="button" class="btn btn-primary" onclick="proceed();">Confirmer</button>
			      </div>
			    </div>
			  </div>
		</div>
		
		<div class="col-12">
		    <h2 class="my-5 text-center">Listes</h2>
		
		    <div class="row">
		        <ul class="list-group col-12">
		        	<c:forEach items="${listes}" var="l">
		        		<li class="list-group-item d-flex justify-content-between align-items-center">
		        			${l.nom}
		        			<div>
			                    <a href="panier.html" class="badge" title="Commencer ses courses"><i class="material-icons">shopping_cart</i></a>
			                    <a href="#supprimer" class="badge text-danger" title="Supprimer" data-toggle="modal" data-target="#confirmModal"><i class="material-icons">delete</i></a>
		                	</div>
		        		</li>
		        	</c:forEach>
		        </ul>
		    </div>
		</div>
		
	    <!-- Footer -->
	    <footer class="row bg-dark footer-demodule fixed-bottom py-1">
	            <div class="col-lg-4 offset-lg-4 text-center">
	                <a class="btn" href="<%= request.getContextPath() %>/nouvelleliste" title="Créer une nouvelle liste"><i class="material-icons">add</i></a>
	            </div>
	        <!-- /.container -->
	    </footer>
	
	    <!-- Bootstrap core JavaScript -->
	    <script type="text/javascript">
		    function proceed () {
		        var form = document.createElement('form');
		        form.setAttribute('method', 'post');
		        form.setAttribute('action', '<%= request.getContextPath() %>/listes');
		        form.style.display = 'hidden';
		        document.body.appendChild(form)
		        form.submit();
		    }
	    </script>
	    <script src="vendor/jquery/jquery.min.js"></script>
	    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>