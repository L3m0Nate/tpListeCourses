package fr.eni.javaee.tpListeCourses.nav;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.tpListeCourses.bll.ListeManager;
import fr.eni.javaee.tpListeCourses.bo.Liste;

/**
 * Servlet implementation class ListesServlet
 */
@WebServlet("/listes")
public class ListesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ListeManager lm = new ListeManager();
		List<Liste> listes = lm.getAllListe();
		request.setAttribute("listes", listes);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/listes.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
