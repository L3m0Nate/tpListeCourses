package fr.eni.javaee.tpListeCourses.bo;

import java.util.ArrayList;
import java.util.List;

public class Liste {
	
	// Declarations
	private int id;
	private String nom;
	private List<Article> articles = new ArrayList<Article>();
	
	
	// Constructors

	public Liste(int id, String nom, List<Article> articles) {
		this.id = id;
		this.nom = nom;
		this.articles = articles;
	}
	
	public Liste(String nom, List<Article> articles) {
		this.nom = nom;
		this.articles = articles;
	}
	

	public Liste() {}

	/* toString()
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Liste [id=" + id + ", nom=" + nom + ", articles=" + articles + "]";
	}
	
	// Getters setters

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * @return the articles
	 */
	public List<Article> getArticles() {
		return articles;
	}
	
	/**
	 * @param articles the articles to set
	 */
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

}
