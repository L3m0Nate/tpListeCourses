package fr.eni.javaee.tpListeCourses.bo;

public class Article {
	
	// Declarations
	private int id;
	private String nom;
	private int id_liste;
	private boolean coche;
	
	
	// Constructors
	
	public Article(int id, String nom, int id_liste, boolean coche) {
		this.id = id;
		this.nom = nom;
		this.id_liste = id_liste;
		this.coche = coche;
	}
	
	public Article(String nom, int id_liste, boolean coche) {
		this.nom = nom;
		this.id_liste = id_liste;
		this.coche = coche;
	}
	
	public Article(String nom, boolean coche) {
		this.nom = nom;
		this.coche = coche;
	}
	
	
	// Getters setters

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * @return the id_liste
	 */
	public int getId_liste() {
		return id_liste;
	}
	
	/**
	 * @param id_liste the id_liste to set
	 */
	public void setId_liste(int id_liste) {
		this.id_liste = id_liste;
	}
	
	/**
	 * @return the coche
	 */
	public boolean isCoche() {
		return coche;
	}
	
	/**
	 * @param coche the coche to set
	 */
	public void setCoche(boolean coche) {
		this.coche = coche;
	}

}
