package fr.eni.javaee.tpListeCourses.bll;

import java.util.ArrayList;
import java.util.List;

import fr.eni.javaee.tpListeCourses.bo.Liste;
import fr.eni.javaee.tpListeCourses.dal.DALException;
import fr.eni.javaee.tpListeCourses.dal.DAOFactory;
import fr.eni.javaee.tpListeCourses.dal.ListeDAO;

public class ListeManager {

	ListeDAO dao;
	
	public ListeManager() {
		this.dao = DAOFactory.getListeDAO();
	}

	public void creerListe(Liste liste) {
		try {
			dao.insert(liste);
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
	
	public Liste getListe(int id) {
		Liste liste = new Liste();
		try {
			dao.select(id);
		} catch (DALException e) {
			e.printStackTrace();
		}
		return liste;
	}
	
	public void supprimerListe(Liste liste) {
		try {
			dao.delete(liste);
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
	
	public List<Liste> getAllListe() {
		List<Liste> listes = new ArrayList<Liste>();
		try {
			return dao.selectAll();
		} catch (DALException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listes;
	}

}
