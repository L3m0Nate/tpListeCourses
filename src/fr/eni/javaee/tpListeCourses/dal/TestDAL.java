package fr.eni.javaee.tpListeCourses.dal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.tpListeCourses.bo.Article;
import fr.eni.javaee.tpListeCourses.bo.Liste;

@WebServlet("/testDAL")
public class TestDAL extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ListeDAO dao = DAOFactory.getListeDAO();
		
	/* Test de l'insertion
		Article a1 = new Article("a1", false);
		Article a2 = new Article("a2", false);
		Article a3 = new Article("a3", false);
		
		List<Article> arts = new ArrayList<Article>();
		arts.add(a1);
		arts.add(a2);
		arts.add(a3);

		Liste l1 = new Liste("Test", arts);
		
		try {
			dao.insert(l1);
		} catch (DALException e) {
			e.printStackTrace();
		}
		
		System.out.println(l1.getId() + " " + l1.getNom());
		for (Article art : arts) {
			System.out.println(art.getId() + " " + art.getNom());
		}
	*/
		
		try {
			Liste liste = dao.select(2);
			System.out.println(liste);
			dao.delete(liste);
		} catch (DALException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
