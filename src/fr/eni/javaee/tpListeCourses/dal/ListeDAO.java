package fr.eni.javaee.tpListeCourses.dal;

import java.util.List;

import fr.eni.javaee.tpListeCourses.bo.Liste;

public interface ListeDAO {

	public void insert(Liste liste) throws DALException;
	public Liste select(int id) throws DALException;
	public void delete(Liste liste) throws DALException;
	public List<Liste> selectAll() throws DALException;
}
