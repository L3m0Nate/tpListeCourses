package fr.eni.javaee.tpListeCourses.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eni.javaee.tpListeCourses.dal.ConnectionProvider;
import fr.eni.javaee.tpListeCourses.bo.Article;
import fr.eni.javaee.tpListeCourses.bo.Liste;

public class ListeDAOJdbcImpl implements ListeDAO {
	
	private static final String INSERT = "insert into listes (nom) values (?);";
	private static final String INSERT_ART = "insert into articles (nom, id_liste, coche) values (?, ?, ?);";
	private static final String SELECT = "select * from listes where id = ?;";
	private static final String SELECT_ART = "select * from articles where id_liste = ?;";
	private static final String SELECT_ALL = "select * from listes;";
	private static final String DELETE = "delete from listes where id = ?;";
	
	public void insert(Liste liste) throws DALException {
		
		if (liste == null) {
			throw new DALException("L'objet liste est null");
		}
		
		try(Connection cnx = ConnectionProvider.getConnection())
		{
			// Insertion de la liste
			PreparedStatement query = cnx.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
			query.setString(1, liste.getNom());
			query.executeUpdate();
			
			ResultSet rs = query.getGeneratedKeys();
			if(rs.next())
			{
				liste.setId(rs.getInt(1));
			}
			
			// Insertion des articles
			for (Article art : liste.getArticles()) {
				PreparedStatement query_art = cnx.prepareStatement(INSERT_ART, PreparedStatement.RETURN_GENERATED_KEYS);
				query_art.setString(1, art.getNom());
				query_art.setInt(2, liste.getId());
				query_art.setBoolean(3, art.isCoche());
				query_art.executeUpdate();
				
				ResultSet rs_art = query_art.getGeneratedKeys();
				if (rs_art.next()) {
					art.setId(rs_art.getInt(1));
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new DALException("Erreur lors de l'insertion dans la bdd");
		}
	}
	
	public Liste select(int id) throws DALException {
		
		List<Article> articles = new ArrayList<Article>();
		
		try(Connection cnx = ConnectionProvider.getConnection())
		{
			// Récupération des articles
			PreparedStatement query_art = cnx.prepareStatement(SELECT_ART);
			query_art.setInt(1, id);
			
			ResultSet rs_art = query_art.executeQuery();
			while (rs_art.next()) {
				Article art = new Article(rs_art.getInt("id"), rs_art.getString("nom"), id, rs_art.getBoolean("coche"));
				articles.add(art);
			}
			
			// Récupération de la liste
			PreparedStatement query = cnx.prepareStatement(SELECT);
			query.setInt(1, id);
			
			ResultSet rs = query.executeQuery();
			if (rs.next()) {
				Liste liste = new Liste(id, rs.getString("nom"), articles);
				return liste;
			} else {
				throw new DALException("Cette liste n'existe pas");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new DALException("Erreur lors de la sélection de la liste");
		}
	}
	
	public void delete(Liste liste) throws DALException {
		
		if (liste == null) {
			throw new DALException("L'objet liste est null");
		}
		
		try(Connection cnx = ConnectionProvider.getConnection())
		{
			PreparedStatement query = cnx.prepareStatement(DELETE);
			query.setInt(1, liste.getId());
			query.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new DALException("Erreur lors de la sélection de la liste");
		}
	}
	
	public List<Liste> selectAll() throws DALException {
		
		List<Liste> listes = new ArrayList<Liste>();
		
		try(Connection cnx = ConnectionProvider.getConnection())
		{
			// Récupération des listes
			Statement query = cnx.createStatement();
			ResultSet rs = query.executeQuery(SELECT_ALL);
			
			while (rs.next()) {
				List<Article> articles = new ArrayList<Article>();
				Liste liste = new Liste(rs.getInt("id"), rs.getString("nom"), articles);
				
				// Récupération des articles de la liste
				PreparedStatement query_art = cnx.prepareStatement(SELECT_ART);
				query_art.setInt(1, liste.getId());
				
				ResultSet rs_art = query_art.executeQuery();
				while (rs_art.next()) {
					Article art = new Article(
						rs_art.getInt("id"),
						rs_art.getString("nom"),
						liste.getId(),
						rs_art.getBoolean("coche")
					);
					articles.add(art);
				}
				
				listes.add(liste);
			}
			
			return listes;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new DALException("Erreur lors de la sélection de la liste");
		}
		
	}

}
